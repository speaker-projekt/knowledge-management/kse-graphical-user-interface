package de.dfki.speaker.ksegui;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;
import com.mashape.unirest.request.body.RawBody;
import com.mashape.unirest.request.body.RequestBodyEntity;

import de.dfki.speaker.kse.data.Collection;
import de.dfki.speaker.kse.data.LDDocument;
import de.dfki.speaker.kse.data.Triple;

@Component
public class KSEProxy {

	public String createCollection(String collectionId) {
		HttpResponse<JsonNode> jsonResponse = null;
		try {
			JSONObject param = new JSONObject("{\"collection\":\"kse2\"}");
			jsonResponse = Unirest.post("http://localhost:8084/kse/createCollection")
					  .field("request",param.toString())
					  .asJson();
			if(jsonResponse.getStatus()==200) {
				System.out.println("JSON1:" + jsonResponse.getBody());
				JSONObject json = new JSONObject(jsonResponse.getBody().toString());
				System.out.println("JSON2: " + json.toString());
				return json.toString();
			}
			else {
				System.out.println("ERROR "+jsonResponse.getStatus()+" Connecting to KSE.");
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;		
	}

	public String createCollection(String collectionId, String collectionName, String collectionDescription) {
		HttpResponse<String> response = null;
		try {
			JSONObject param = new JSONObject();
			param.put("collection", collectionId);
			param.put("collectionName", collectionName);
			param.put("collectionDesc", collectionDescription);
			response = Unirest.post("http://localhost:8084/kse/createCollection")
					  .field("request",param.toString())
					  .asString();
			if(response.getStatus()==200) {
				System.out.println("JSON1:" + response.getBody());
//				JSONObject json = new JSONObject(jsonResponse.getBody().toString());
//				System.out.println("JSON2: " + json.toString());
				return response.getBody();
			}
			else {
				System.out.println("ERROR "+response.getStatus()+" Connecting to KSE.");
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;		
	}

	public List<Collection> getCollections() {
		List<Collection> collections = new LinkedList<Collection>();
		HttpResponse<JsonNode> jsonResponse = null;
		try {
			jsonResponse = Unirest.get("http://localhost:8084/kse/listCollections")
					  .asJson();
			if(jsonResponse.getStatus()==200) {
				System.out.println("JSON1:" + jsonResponse.getBody());
				JSONArray array = new JSONArray(jsonResponse.getBody().toString());
				for (int i = 0; i < array.length(); i++) {
					JSONObject json = array.getJSONObject(i);
					Collection c = new Collection();
					c.collectionId = json.getString("collectionId");
					c.name = json.getString("collectionName");
					c.collectionDescription = json.getString("collectionDescription");
					collections.add(c);					
				}
				System.out.println("JSON2: " + array.toString());
				return collections;
			}
			else {
				System.out.println("ERROR "+jsonResponse.getStatus()+" Connecting to KSE.");
			}
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return collections;
	}

	public JSONObject getCollection(String collectionId) {
		HttpResponse<JsonNode> jsonResponse = null;
		try {
			jsonResponse = Unirest.get("http://localhost:8084/kse/getCollection?collectionId="+collectionId)
//					  .queryString("collectionId",collectionId)
					  .asJson();
			if(jsonResponse.getStatus()==200) {
				System.out.println("JSON1:" + jsonResponse.getBody());
				JSONObject json = new JSONObject(jsonResponse.getBody().toString());
				System.out.println("JSON2: " + json.toString());
				return json;
			}
			else {
				System.out.println("ERROR "+jsonResponse.getStatus()+" Connecting to KSE.");
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		Collection c = new Collection();
//		c.collectionId = "collection1";
//		c.name = "Collection 1";
//		c.collectionDescription = "This is an example collection to show the interface capabilities.";
//		return c;
		return null;
	}

	public JSONObject getDocument(String documentId) {
		HttpResponse<JsonNode> jsonResponse = null;
		try {
//			JSONObject param = new JSONObject("{\"documentId\":\"http://kse.dfki.de/ontology/doc/c9269b0a09d5328e4ad2838bee69142a0cd748acc3822809cbe700fc1b5b23c2\"}");
			JSONObject param = new JSONObject();
			param.put("documentId", documentId);
			jsonResponse = Unirest.post("http://localhost:8084/kse/retrieveDocument")
					  .field("request",param.toString())
					  .asJson();
			if(jsonResponse.getStatus()==200) {
				System.out.println("JSON1:" + jsonResponse.getBody());
				JSONObject json = new JSONObject(jsonResponse.getBody().toString());
				System.out.println("JSON2: " + json.toString());
				return json;
			}
			else {
				System.out.println("ERROR "+jsonResponse.getStatus()+" Connecting to KSE: "+jsonResponse.getBody());
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String source = "[\n" + 
				"		    {\n" + 
				"		        \"triples\": [],\n" + 
				"		        \"documentId\": \"http://kse.dfki.de/ontology/doc/c9269b0a09d5328e4ad2838bee69142a0cd748acc3822809cbe700fc1b5b23c2\",\n" + 
				"		        \"text\": \"2222 That is the text to do something with.\",\n" + 
				"		        \"sourcePath\": null\n" + 
				"		    }\n" + 
				"		]";
		JSONObject json = new JSONObject(source);
		return json;
	}
	
	public String addDocument(String collectionId, String docText, String docTriples, MultipartFile file) {
		try {
			JSONObject param = new JSONObject();
			param.put("text", docText);
			param.put("collection", collectionId);
			JSONArray triples = new JSONArray();
			docTriples = docTriples.trim();
			if(!docTriples.equalsIgnoreCase("")) {
				String rows[] = docTriples.split("\n");
				for (String triple : rows) {
					String elems[] = triple.split(" ");
					JSONObject json = new JSONObject();
					json.put("subject", elems[0]);
					json.put("predicate", elems[1]);
					json.put("object", elems[2]);
					triples.put(json);
				}
				param.put("triples", triples);
			}
			File tmpFile = null;
			try {
				tmpFile = new File(System.getProperty("java.io.tmpdir")+"/"+file.getOriginalFilename());
				file.transferTo(tmpFile);
			} catch (Exception e) {
				// TODO: handle exception
			}

			//JSONObject param = new JSONObject("{\"documentId\":\"http://kse.dfki.de/ontology/doc/c9269b0a09d5328e4ad2838bee69142a0cd748acc3822809cbe700fc1b5b23c2\"}");
			System.out.println(param.toString());
			System.out.println(file);
		    MultipartBody body = null;
		    if(tmpFile==null) {
		    	body = Unirest.post("http://localhost:8084/kse/createDocument")
			            .field("request", param.toString());
		    }
		    else {
		    	body = Unirest.post("http://localhost:8084/kse/createDocument")
			            .field("request", param.toString())
			            .field("content", tmpFile);
		    }
		    HttpResponse<String> response = body.asString();
		    System.out.println(response.getStatus());
			if(response.getStatus()==200) {
				System.out.println("JSON1:" + response.getBody());
				JSONObject json = new JSONObject(response.getBody().toString());
				System.out.println("JSON2: " + json.toString());
				return response.getBody();
			}
			else {
				System.out.println("ERROR "+response.getStatus()+" Connecting to KSE.");
			}
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return "Error: not added.";
	}
	
	
	public String search(String query,String collection){
		Date d1 = new Date();
		HttpResponse<JsonNode> jsonResponse = null;
		try {
			JSONObject param = new JSONObject();
			if(collection!=null) {
				param.put("collection", collection);
			}
			param.put("query", query);
			jsonResponse = Unirest.post("http://localhost:8084/kse/search")
					  .field("request",param.toString())
					  .asJson();
			if(jsonResponse.getStatus()==200) {
				System.out.println("JSON1:" + jsonResponse.getBody());
				JSONArray json = new JSONArray(jsonResponse.getBody().toString());
				System.out.println("JSON2: " + json.toString());
			}
			else {
				System.out.println("ERROR "+jsonResponse.getStatus()+" Connecting to KSE.");
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String source = "[\n" + 
				"		    {\n" + 
				"		        \"triples\": [],\n" + 
				"		        \"documentId\": \"http://kse.dfki.de/ontology/doc/c9269b0a09d5328e4ad2838bee69142a0cd748acc3822809cbe700fc1b5b23c2\",\n" + 
				"		        \"text\": \"That is the text to do something with.\",\n" + 
				"		        \"sourcePath\": null\n" + 
				"		    }\n" + 
				"		]";
//		JSONArray array = new JSONArray(source);
		JSONArray array = new JSONArray(jsonResponse.getBody().toString());
		
		Date d2 = new Date();
		long miliseconds = d2.getTime() - d1.getTime();
		float time = ((float)(miliseconds))/1000;
		int numResults = array.length();
		
		String results = "       <div class=\"panel panel-default\">\n" + 
				"						<div class=\"panel-body\">\n" + 
				"							<h6 class=\"nomargin\">\n" + 
				"								About "+numResults+" results <small class=\"text-primary\">("+time+"\n" + 
				"									seconds) </small>\n" + 
				"							</h6>\n" + 
				"							<hr class=\"nomargin-bottom margin-top-10\">\n" + 
				"							<!-- SEARCH RESULTS -->\n";
		for (int i = 0; i < array.length(); i++) {
			JSONObject json = array.getJSONObject(i);
			String title = json.getString("text");
			String docid = json.getString("documentId");
			String snippet = json.getString("text");
			results = results + 
					"							<div class=\"clearfix search-result\">\n" + 
					"								<h4>\n" + 
//					"									<a href=\"#\">"+title+"</a>\n" + 
					"									<a href=\"document?docid="+docid+"\">"+
					title+"</a>\n" + 
					"								</h4>\n" + 
					"								<small class=\"text-success\">"+docid+"/</small>\n" + 
					"								<p>"+snippet+"</p>\n" + 
					"							</div>\n" + 
					"\n";
		}
//		/**
//		 * Pagination
//		 */
//		results = results + "\n" + 
//		"							<!-- PAGINATION -->\n" + 
//		"							<div class=\"text-center margin-top-20\">\n" + 
//		"								<nav aria-label=\"Page navigation example\">\n" + 
//		"									<ul class=\"pagination\">\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">Previous</a></li>\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">1</a></li>\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\n" + 
//		"									</ul>\n" + 
//		"								</nav>\n" + 
//		"							</div>\n" + 
//		"							<!-- /PAGINATION -->\n" + 
//		"";
		/**
		 * Ending tags
		 */
		results = results +  
		"						</div>\n" + 
		"					</div>\n" + 
		"";
		return results;
	}
	
	public String sparql(String query){
		Date d1 = new Date();
		/**
		 *  TODO Establish connection with REST API KSE.
		 */
		
		List<LDDocument> docs = new LinkedList<LDDocument>();
		List<String> params = new LinkedList<String>();
		List<HashMap<String, String>> hashmaps = new LinkedList<HashMap<String,String>>();
		
		Date d2 = new Date();
		long miliseconds = d2.getTime() - d1.getTime();
		float time = ((float)(miliseconds))/1000;
		int numResults = docs.size();
		
		String results = "       <div class=\"panel panel-default\">\n" + 
				"						<div class=\"panel-body\">\n" + 
				"							<h6 class=\"nomargin\">\n" + 
				"								About "+numResults+" results <small class=\"text-primary\">("+time+"\n" + 
				"									seconds) </small>\n" + 
				"							</h6>\n" + 
				"							<hr class=\"nomargin-bottom margin-top-10\">\n" + 
				"							<!-- SEARCH RESULTS -->\n";

		
		String head = "  <thead>\n" + 
				"    <tr>\n" + 
				"      <th scope=\"col\">#</th>\n";
		for (String s : params) {
			head = head + "      <th scope=\"col\">"+s+"</th>\n";
		}
		head = head + "    </tr>\n" + 
				"  </thead>\n"; 
		String body = "  <tbody>\n";
		int counter = 1;
		for (HashMap<String, String> map : hashmaps) {
			body = body + "    <tr>\n";
			body = body + "      <th scope=\"row\">"+counter+"</th>\n"; 
			for (String s : map.keySet()) {
				body = body + "      <td>"+map.get(s)+"</td>\n";
			}
			body = body + "    </tr>\n";
			counter++;
		}
		body = body + "  </tbody>\n";
		String table = "<table class=\"table\">\n";
		table = table + head;
		table = table + body;
		table = table + "</table>";
		
		results = results + table;
//		/**
//		 * Pagination
//		 */
//		results = results + "\n" + 
//		"							<!-- PAGINATION -->\n" + 
//		"							<div class=\"text-center margin-top-20\">\n" + 
//		"								<nav aria-label=\"Page navigation example\">\n" + 
//		"									<ul class=\"pagination\">\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">Previous</a></li>\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">1</a></li>\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n" + 
//		"										<li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\n" + 
//		"									</ul>\n" + 
//		"								</nav>\n" + 
//		"							</div>\n" + 
//		"							<!-- /PAGINATION -->\n" + 
//		"";
		/**
		 * Ending tags
		 */
		results = results +  
		"						</div>\n" + 
		"					</div>\n" + 
		"";
		return results;
	}

	public static void main(String[] args) {
//		HttpResponse<JsonNode> jsonResponse = null;
//		try {
//			JSONObject param = new JSONObject("{\"documentId\":\"http://kse.dfki.de/ontology/doc/c9269b0a09d5328e4ad2838bee69142a0cd748acc3822809cbe700fc1b5b23c2\"}");
//			jsonResponse = Unirest.post("http://localhost:8084/kse/retrieveDocument")
//			  .field("request",param.toString())
//			  .asJson();
//			if(jsonResponse.getStatus()==200) {
//				System.out.println(jsonResponse.getBody());
//				JSONObject json = new JSONObject(jsonResponse.getBody());
//				System.out.println("RESULT:" + json);
//			}
//			else {
//				System.out.println("ERROR "+jsonResponse.getStatus()+" Connecting to KSE.");
//			}
//		} catch (UnirestException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		String collectionId = "kse";
		HttpResponse<String> jsonResponse = null;
		try {
			jsonResponse = Unirest.get("http://localhost:8084/kse/getCollection")
					  .queryString("collectionId",collectionId)
					  .asString();
			if(jsonResponse.getStatus()==200) {
				System.out.println("JSON1:" + jsonResponse.getBody());
				JSONObject json = new JSONObject(jsonResponse.getBody().toString());
				System.out.println("JSON2: " + json.toString());
			}
			else {
				System.out.println("ERROR "+jsonResponse.getStatus()+" Connecting to KSE.");
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
