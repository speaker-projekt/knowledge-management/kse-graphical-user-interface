package de.dfki.speaker.ksegui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.dfki.speaker.ksegui"})
public class KSEGUIApplication {

    public static void main(String[] args) {
        SpringApplication.run(KSEGUIApplication.class, args);
    } 

}
