package de.dfki.speaker.ksegui;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("/kse/mngt")
@SpringBootApplication(exclude = {
		org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
//,
//        org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration.class}
//        )
@OpenAPIDefinition(/*servers = {@Server(url = "https://apis.lynx-project.eu/api/named-entity-recognition")},*/
		info = @Info(
				title = "Graphical User Interface for Knowledge Storage Ecosystem",
				version = "2.0.1",
				description = "",
				contact = @Contact(name = "SPEAKER DFKI Team",
				email = "speaker-project@dfki.de")))
public class RestAPIController {

	Logger logger = Logger.getLogger(RestAPIController.class);

	@Operation(
			summary = "",
			responses = {
					@ApiResponse(responseCode = "200",
							description = "Message stating that the property has been properly added to KSE.",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
					@ApiResponse(responseCode = "400",
					description = "Bad request.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)),
					@ApiResponse(responseCode = "500",
					description = "An error has ocurred in the server.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE))
			})
	@CrossOrigin
	@RequestMapping(value = "/addProperty",method = {	RequestMethod.POST })
	public ResponseEntity<String> addProperty(
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
//			@RequestParam(value = "content",required = false) MultipartFile content,
//			@RequestParam(value = "request",required = false) String jsonRequest,
			@RequestBody(required = true) String postBody) throws Exception {
		//System.out.println(file.getContentType());

		JSONObject json = new JSONObject(postBody);		

		//TODO 
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/plain");
		String responseText = "To be Implemented.";
		ResponseEntity<String> response = new ResponseEntity<String>(responseText, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		return response;
	}

	@Operation(
			summary = "",
			responses = {
					@ApiResponse(responseCode = "200",
							description = "Message containing the list of properties based on the input query, if provided.",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
					@ApiResponse(responseCode = "400",
					description = "Bad request.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)),
					@ApiResponse(responseCode = "500",
					description = "An error has ocurred in the server.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE))
			})
	@CrossOrigin
	@RequestMapping(value = "/listProperties",method = {	RequestMethod.GET})
	public ResponseEntity<String> listProperties(
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
			@RequestParam(value = "filter",required = false) String filter,
			@RequestBody(required = true) String postBody) throws Exception {

		JSONObject json = new JSONObject(postBody);		

		//TODO 
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/plain");
		String responseText = "To be Implemented.";
		ResponseEntity<String> response = new ResponseEntity<String>(responseText, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		return response;
	}

	@Operation(
			summary = "",
			responses = {
					@ApiResponse(responseCode = "200",
							description = "Message stating if the property was correctly deleted in KSE.",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
					@ApiResponse(responseCode = "400",
					description = "Bad request.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)),
					@ApiResponse(responseCode = "500",
					description = "An error has ocurred in the server.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE))
			})
	@CrossOrigin
	@RequestMapping(value = "/deleteProperties",method = {	RequestMethod.DELETE})
	public ResponseEntity<String> deleteProperties(
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
			@RequestParam(value = "filter",required = false) String filter,
			@RequestBody(required = true) String postBody) throws Exception {

		JSONObject json = new JSONObject(postBody);		

		//TODO 
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/plain");
		String responseText = "To be Implemented.";
		ResponseEntity<String> response = new ResponseEntity<String>(responseText, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		return response;
	}

	@Operation(
			summary = "",
			responses = {
					@ApiResponse(responseCode = "200",
							description = "Message stating if the property was correctly modified in KSE.",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
					@ApiResponse(responseCode = "400",
					description = "Bad request.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)),
					@ApiResponse(responseCode = "500",
					description = "An error has ocurred in the server.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE))
			})
	@CrossOrigin
	@RequestMapping(value = "/modifyProperties",method = {	RequestMethod.PUT})
	public ResponseEntity<String> modifyProperties(
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
			@RequestParam(value = "filter",required = false) String filter,
			@RequestBody(required = true) String postBody) throws Exception {

		JSONObject json = new JSONObject(postBody);		

		//TODO 
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/plain");
		String responseText = "To be Implemented.";
		ResponseEntity<String> response = new ResponseEntity<String>(responseText, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		return response;
	}

	@Operation(hidden=true)
	@RequestMapping(value = "/testURL", method = { RequestMethod.POST, RequestMethod.GET })
	public ResponseEntity<String> testURL(
			@RequestBody(required = false) String postBody) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/plain");
		ResponseEntity<String> response = new ResponseEntity<String>("The KSE_GUI restcontroller is working properly", responseHeaders, HttpStatus.OK);
		return response;
	}

}
