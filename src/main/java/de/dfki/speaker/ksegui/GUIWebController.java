package de.dfki.speaker.ksegui;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import de.dfki.speaker.kse.data.Collection;
import de.dfki.speaker.kse.data.LDDocument;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Controller
@RequestMapping("/kse/gui")
@SpringBootApplication(exclude = {
		org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
public class GUIWebController {

	String footerText = "&copy 2022 PanQURA Project. All Rights Reserved";
	String menuLogo= "<img src=\"/images/KSE_logo.png\" style=\"max-height:30px;\"/>"; 
	
	String menuText = "" + 
			"<li class=\"nav-item active\">\n" + 
			"  <a class=\"nav-link\" href=\"/kse/gui\">Home</a>\n" + 
			"</li>\n" + 
			"<li class=\"nav-item\">\n" + 
			"  <a class=\"nav-link\" href=\"/kse/gui/management\">Management</a>\n" + 
			"</li>\n" + 
			"<li class=\"nav-item dropdown\">\n" + 
			"  <a class=\"nav-link dropdown-toggle\" id=\"dropdown01\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Knowledge</a>\n" + 
			"  <div class=\"dropdown-menu\" aria-labelledby=\"dropdown01\">\n" + 
			"    <a class=\"dropdown-item\" href=\"/kse/gui/collections\">Collections</a>\n" + 
			"    <a class=\"dropdown-item\" href=\"/kse/gui/sparqlsite\">Sparql</a>\n" + 
			"    <a class=\"dropdown-item\" href=\"/kse/gui/searchsite\">Search</a>\n" + 
			"  </div>\n" + 
			"</li>\n" + 
			"";
	
	@Autowired
	KSEProxy kseProxy;
	
    @GetMapping("/chatbot")
    public String chatbot(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
        return "chatbot";
    }
    
    
    
    /**
     * @return The index website of the GUI for the Workflow Manager
     */
    @GetMapping("/")
    public String greeting(org.springframework.ui.Model model) {
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
        return "index";
    }

    /**
     * @return The index website of the GUI for the Workflow Manager
     */
    @GetMapping("")
    public String index(org.springframework.ui.Model model) {
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
        return "index";
    }

    public class User{
    	public String id;
    	public String key;
    	public String value;
		public User(String id, String key, String value) {
			super();
			this.id = id;
			this.key = key;
			this.value = value;
		}
    }
    
    public class Prop{
    	public String id;
    	public String key;
    	public String value;
		public Prop(String id, String key, String value) {
			super();
			this.id = id;
			this.key = key;
			this.value = value;
		}
    }
    
    @GetMapping("/management")
    public String management(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
    	model.addAttribute("controllers", "");
    	model.addAttribute("templates", "");
    	model.addAttribute("executions", "");
    	
    	String conf = FileUtils.readFileToString(new File("src/main/resources/management/config.json"),"utf-8");
    	JSONObject jConf = new JSONObject(conf);
    	
    	List<GUIWebController.User> users = new LinkedList<GUIWebController.User>();
    	JSONArray usersArray = jConf.getJSONArray("users");
    	for (int i = 0; i < usersArray.length(); i++) {
    		JSONObject j1 = usersArray.getJSONObject(i);
    		
    		User u = new User((i+1)+"", j1.getString("name"), j1.getString("password"));
    		users.add(u);
		}
    	model.addAttribute("users", users);
    	List<Prop> props1 = new LinkedList<GUIWebController.Prop>();
    	JSONObject jProps1 = jConf.getJSONObject("triple_store");
    	Iterator<String> it = jProps1.keys();
    	for (int i = 0; it.hasNext(); i++) {
    		String k = it.next();
    		Prop p = new Prop((i+1)+"", k, jProps1.getString(k));
    		props1.add(p);
		}
    	model.addAttribute("ts_properties", props1);
    	
    	List<Prop> props2 = new LinkedList<GUIWebController.Prop>();
    	JSONObject jProps2 = jConf.getJSONObject("file_system");
    	Iterator<String> it2 = jProps2.keys();
    	for (int i = 0; it2.hasNext(); i++) {
    		String k = it2.next();
    		Prop p = new Prop((i+1)+"", k, jProps2.getString(k));
    		props2.add(p);
		}
    	model.addAttribute("fs_properties", props2);

    	List<Prop> props3 = new LinkedList<GUIWebController.Prop>();
    	JSONObject jProps3 = jConf.getJSONObject("text_indexes");
    	Iterator<String> it3 = jProps3.keys();
    	for (int i = 0; it3.hasNext(); i++) {
    		String k = it3.next();
    		Prop p = new Prop((i+1)+"", k, jProps3.getString(k));
    		props3.add(p);
		}
    	model.addAttribute("ti_properties", props3);
    	

//    	System.out.println("We arrive here.");
//    	try {
//    		List<de.dfki.cwm.controllers.Controller> controllers = engine.listControllersObject(null);
//    		model.addAttribute("contros", controllers);
//    		String controllersHTML = "";
//    		for (de.dfki.cwm.controllers.Controller controller : controllers) {
//    			controllersHTML += controller.getControllerId() + "--" + controller.getControllerName()  + "--" + controller.getName()  + "<br/>";
//    		}
//    		model.addAttribute("controllers", controllersHTML);
//
//
//    		List<WorkflowTemplate> templates = engine.listWorkflowTemplatesObject(null);
//    		model.addAttribute("templas", templates);
//    		String templatesHTML = "";
//    		for (WorkflowTemplate template : templates) {				
//    			templatesHTML += template.getName() + "--" + template.getWorkflowId()  + "--" + template.getWorkflowTemplateId()  + "<br/>";
//    		}
//    		model.addAttribute("templates", templatesHTML);
//
//    		List<WorkflowExecution> workflows = engine.listWorkflowExecutionsObject(null);
//    		model.addAttribute("execus", workflows);
//    		String workflowsHTML = "";
//    		for (WorkflowExecution execution : workflows) {
//    			workflowsHTML += execution.getOutput();
//    		}
//    		model.addAttribute("executions", workflowsHTML);
//    	}
//    	catch(Exception e) {
//    		e.printStackTrace();
//    	}
//    	System.out.println("Here too.");
    	return "management";
    }

    
    @GetMapping("/management/users")
    public String managementUsers(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
    	model.addAttribute("controllers", "");
    	model.addAttribute("templates", "");
    	model.addAttribute("executions", "");
    	
    	String conf = FileUtils.readFileToString(new File("src/main/resources/management/config.json"),"utf-8");
    	JSONObject jConf = new JSONObject(conf);
    	
    	List<GUIWebController.User> users = new LinkedList<GUIWebController.User>();
    	JSONArray usersArray = jConf.getJSONArray("users");
    	for (int i = 0; i < usersArray.length(); i++) {
    		JSONObject j1 = usersArray.getJSONObject(i);
    		
    		User u = new User((i+1)+"", j1.getString("name"), j1.getString("password"));
    		users.add(u);
		}
    	model.addAttribute("users", users);
    	return "users";
    }

    @GetMapping("/management/config")
    public String managementTriple(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
    	model.addAttribute("controllers", "");
    	model.addAttribute("templates", "");
    	model.addAttribute("executions", "");
    	
    	String conf = FileUtils.readFileToString(new File("src/main/resources/management/config.json"),"utf-8");
    	JSONObject jConf = new JSONObject(conf);
    	
    	List<Prop> props1 = new LinkedList<GUIWebController.Prop>();
    	JSONObject jProps1 = jConf.getJSONObject("triple_store");
    	Iterator<String> it = jProps1.keys();
    	for (int i = 0; it.hasNext(); i++) {
    		String k = it.next();
    		Prop p = new Prop((i+1)+"", k, jProps1.getString(k));
    		props1.add(p);
		}
    	model.addAttribute("ts_properties", props1);
    	List<Prop> props2 = new LinkedList<GUIWebController.Prop>();
    	JSONObject jProps2 = jConf.getJSONObject("file_system");
    	Iterator<String> it2 = jProps2.keys();
    	for (int i = 0; it2.hasNext(); i++) {
    		String k = it2.next();
    		Prop p = new Prop((i+1)+"", k, jProps2.getString(k));
    		props2.add(p);
		}
    	model.addAttribute("fs_properties", props2);

    	List<Prop> props3 = new LinkedList<GUIWebController.Prop>();
    	JSONObject jProps3 = jConf.getJSONObject("text_indexes");
    	Iterator<String> it3 = jProps3.keys();
    	for (int i = 0; it3.hasNext(); i++) {
    		String k = it3.next();
    		Prop p = new Prop((i+1)+"", k, jProps3.getString(k));
    		props3.add(p);
		}
    	model.addAttribute("ti_properties", props3);
    	return "config";
    }

    @GetMapping("/collections")
    public String collections(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
    	model.addAttribute("collections", "");
    	model.addAttribute("templates", "");
    	model.addAttribute("executions", "");
//    	System.out.println("We arrive here.");
    	try {
    		List<Collection> collections = kseProxy.getCollections();
    		System.out.println(collections);
    		model.addAttribute("collecs", collections);
//    		String controllersHTML = "";
//    		for (de.dfki.cwm.controllers.Controller controller : controllers) {
//    			controllersHTML += controller.getControllerId() + "--" + controller.getControllerName()  + "--" + controller.getName()  + "<br/>";
//    		}
//    		model.addAttribute("controllers", controllersHTML);
//    		List<WorkflowTemplate> templates = engine.listWorkflowTemplatesObject(null);
//    		model.addAttribute("templas", templates);
//    		String templatesHTML = "";
//    		for (WorkflowTemplate template : templates) {				
//    			templatesHTML += template.getName() + "--" + template.getWorkflowId()  + "--" + template.getWorkflowTemplateId()  + "<br/>";
//    		}
//    		model.addAttribute("templates", templatesHTML);
//
//    		List<WorkflowExecution> workflows = engine.listWorkflowExecutionsObject(null);
//    		model.addAttribute("execus", workflows);
//    		String workflowsHTML = "";
//    		for (WorkflowExecution execution : workflows) {
//    			workflowsHTML += execution.getOutput();
//    		}
//    		model.addAttribute("executions", workflowsHTML);
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
//    	System.out.println("Here too.");
    	return "collections";
    }

    @GetMapping("/collection")
    public String collection(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	try {
        	model.addAttribute("footertext", footerText);
        	model.addAttribute("menutext", menuText);
        	model.addAttribute("menulogo", menuLogo);
    		String collectionId = request.getParameter("collectionId");
    		System.out.println(collectionId);
    		model.addAttribute("collectionId", collectionId);
    		JSONObject json = kseProxy.getCollection(collectionId);
    		System.out.println(json.toString(4));
    		model.addAttribute("collectionName", json.getString("collectionName"));
    		model.addAttribute("numDocs", json.get("numDocs"));
    		model.addAttribute("numTriples", json.get("numTriples"));
    		model.addAttribute("numIndexes", json.get("numIndexes"));
    		JSONArray array = json.getJSONArray("documents");
    		String table = "<table class=\"table\">\n" +
    				"  <thead>\n" + 
    				"    <tr>\n" + 
    				"      <th scope=\"col\">#</th>\n" + 
    				"      <th scope=\"col\">Document Identifier</th>\n" + 
    				"    </tr>\n" + 
    				"  </thead>\n";
    		String body = "  <tbody>\n";
    		int counter = 1;
    		for (int i = 0; i < array.length(); i++) {
				String s = array.getString(i);
    			body = body + "    <tr>\n";
    			body = body + "      <th scope=\"row\">"+counter+"</th>\n"; 
    			body = body + "      <td><a href=\"document?docid="+s+"\">"+s+"</a></td>\n";
    			body = body + "    </tr>\n";
    			counter++;
			}
    		body = body + "  </tbody>\n";
    		table = table + body;
    		table = table + "</table>";
    		model.addAttribute("documents", table);
       	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    	return "collection";
    }

	@CrossOrigin
	@RequestMapping(value = "/createCollection",method = {	RequestMethod.POST })
//    public String addDocument(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
	public String createCollection(
			HttpServletResponse httpServletResponse,
			org.springframework.ui.Model model,
			HttpServletRequest request,
			@RequestParam(value = "docFile",required = false) MultipartFile file){
    	try {
        	model.addAttribute("footertext", footerText);
        	model.addAttribute("menutext", menuText);
        	model.addAttribute("menulogo", menuLogo);

        	String collectionId = request.getParameter("colId");
    		System.out.println("collectionId: " + collectionId);
    		model.addAttribute("collectionId", collectionId);
    		String collectionName = request.getParameter("colName");
    		String collectionDescription = request.getParameter("colDesc");

    		System.out.println("DEBUG: colname: " + collectionName);
    		System.out.println("DEBUG: colid" + collectionId);
    		System.out.println("DEBUG: colDesc:" + collectionDescription);
    		
    		String doc = kseProxy.createCollection(collectionId,collectionName,collectionDescription);
//    		model.addAttribute("documentText", doc.getString("text"));
//    	    httpServletResponse.setHeader("Location", "collections");
//    	    httpServletResponse.setStatus(302);
    		List<Collection> collections = kseProxy.getCollections();
    		System.out.println(collections);
    		model.addAttribute("collecs", collections);

        	return "collections";
       	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    	return "collections";
    }

    @GetMapping("/document")
    public String document(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	try {
        	model.addAttribute("footertext", footerText);
        	model.addAttribute("menutext", menuText);
        	model.addAttribute("menulogo", menuLogo);
    		String documentId = request.getParameter("docid");
    		System.out.println("DocumentId: " + documentId);
    		model.addAttribute("documentId", documentId);
    		
    		JSONObject doc = kseProxy.getDocument(documentId);
    		model.addAttribute("documentText", doc.getString("text"));
    		model.addAttribute("sourcePath", doc.getString("sourcePath"));
    		JSONArray triples = doc.getJSONArray("triples");
    		
    		String table = "<table class=\"table\">\n" +
    				"  <thead>\n" + 
    				"    <tr>\n" + 
    				"      <th scope=\"col\">#</th>\n" + 
    				"      <th scope=\"col\">Subject</th>\n" + 
    				"      <th scope=\"col\">Predicate</th>\n" + 
    				"      <th scope=\"col\">Object</th>\n" + 
    				"    </tr>\n" + 
    				"  </thead>\n";
    		String body = "  <tbody>\n";
    		int counter = 1;
    		for (int i = 0; i < triples.length(); i++) {
				JSONObject triple = triples.getJSONObject(i);
    			body = body + "    <tr>\n";
    			body = body + "      <th scope=\"row\">"+counter+"</th>\n"; 
    			body = body + "      <td>"+triple.getString("subject")+"</td>\n";
    			body = body + "      <td>"+triple.getString("predicate")+"</td>\n";
    			body = body + "      <td>"+triple.getString("object")+"</td>\n";
    			body = body + "    </tr>\n";
    			counter++;
			}
    		body = body + "  </tbody>\n";
    		table = table + body;
    		table = table + "</table>";
    		model.addAttribute("documentTriples", table);
       	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    	return "document";
    }

	@CrossOrigin
	@RequestMapping(value = "/addDocument",method = {	RequestMethod.POST })
//    public String addDocument(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
	public void addDocument(
			HttpServletResponse httpServletResponse,
			org.springframework.ui.Model model,
			HttpServletRequest request,
			@RequestParam(value = "docFile",required = false) MultipartFile file){
    	try {
    		System.out.println("ENTRAMOS EN EL METODO");
        	model.addAttribute("footertext", footerText);
        	model.addAttribute("menutext", menuText);
        	model.addAttribute("menulogo", menuLogo);
    		String collectionId = request.getParameter("collectionId");
    		System.out.println("collectionId: " + collectionId);
    		model.addAttribute("collectionId", collectionId);
    		String documentText = request.getParameter("docText");
    		String documentTriples = request.getParameter("docTriples");

    		System.out.println("DEBUG: docname: " + documentText);
    		System.out.println("DEBUG: doctriples" + documentTriples);
    		System.out.println("DEBUG: File Name:" + file.getOriginalFilename());
    		
    		String doc = kseProxy.addDocument(collectionId,documentText,documentTriples,file);
//    		model.addAttribute("documentText", doc.getString("text"));
    	    httpServletResponse.setHeader("Location", "collection?&collectionId="+collectionId);
    	    httpServletResponse.setStatus(302);

        	//return "collection";
       	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
//    	return "collection";
    }

    @GetMapping("/sparqlsite")
    public String sparqlsite(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
//    	System.out.println("We arrive here.");
    	try {
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
//    	System.out.println("Here too.");
    	return "sparqlsite";
    }

    @PostMapping("/sparql")
    public String sparql(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
//    	System.out.println("We arrive here.");
    	try {
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
//    	System.out.println("Here too.");
    	String query = request.getParameter("text");
    	System.out.println("QUERY: " + query);
    	String results = kseProxy.sparql(query);
    	String resultsTitle = "<h1 class=\"display-4\">Results for \""+query+"\"</h1>";
    	model.addAttribute("results", results);
    	model.addAttribute("resultsTitle", resultsTitle);
    	return "sparqlsite";
    }

    @GetMapping("/searchsite")
    public String searchsite(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
//    	System.out.println("We arrive here.");
    	try {
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    	String searchSelect = "";
    	List<Collection> colections = kseProxy.getCollections();
    	for (Collection c : colections) {
    		searchSelect += "<option>"+c.getCollectionId()+"</option>\n";
		}
    	model.addAttribute("searchSelect", searchSelect);
//    	System.out.println("Here too.");
    	return "searchsite";
    }
    
    @PostMapping("/search")
    public String search(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
    	model.addAttribute("footertext", footerText);
    	model.addAttribute("menutext", menuText);
    	model.addAttribute("menulogo", menuLogo);
//    	System.out.println("We arrive here.");
    	try {
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
//    	System.out.println("Here too.");
    	String query = request.getParameter("text");
    	System.out.println("QUERY: " + query);
    	String collection = request.getParameter("collection");
    	String results = kseProxy.search(query,collection);
    	String resultsTitle = "<h1 class=\"display-4\">Results for \""+query+"\"</h1>";
    	model.addAttribute("results", results);
    	model.addAttribute("resultsTitle", resultsTitle);

    	String searchSelect = "";
    	List<Collection> colections = kseProxy.getCollections();
    	for (Collection c : colections) {
    		searchSelect += "<option>"+c.getCollectionId()+"</option>\n";
		}
    	model.addAttribute("searchSelect", searchSelect);
    	return "searchsite";
    }
//    @GetMapping("/list")
//    public String search(org.springframework.ui.Model model,HttpServletRequest request) throws Exception{
//    	model.addAttribute("controllers", "");
//    	model.addAttribute("templates", "");
//    	model.addAttribute("executions", "");
//    	System.out.println("We arrive here.");
//    	try {
//			List<de.dfki.cwm.controllers.Controller> controllers = engine.listControllersObject(null);
//	    	model.addAttribute("contros", controllers);
//			String controllersHTML = "";
//			for (de.dfki.cwm.controllers.Controller controller : controllers) {
//				controllersHTML += controller.getControllerId() + "--" + controller.getControllerName()  + "--" + controller.getName()  + "<br/>";
//			}
//	    	model.addAttribute("controllers", controllersHTML);
//
//	    	
//	    	List<WorkflowTemplate> templates = engine.listWorkflowTemplatesObject(null);
//	    	model.addAttribute("templas", templates);
//			String templatesHTML = "";
//			for (WorkflowTemplate template : templates) {				
//				templatesHTML += template.getName() + "--" + template.getWorkflowId()  + "--" + template.getWorkflowTemplateId()  + "<br/>";
//			}
//	    	model.addAttribute("templates", templatesHTML);
//			
//	    	List<WorkflowExecution> workflows = engine.listWorkflowExecutionsObject(null);
//	    	model.addAttribute("execus", workflows);
//			String workflowsHTML = "";
//	    	for (WorkflowExecution execution : workflows) {
//				workflowsHTML += execution.getOutput();
//			}
//	    	model.addAttribute("executions", workflowsHTML);
//    	}
//    	catch(Exception e) {
//    		e.printStackTrace();
//    	}
//    	System.out.println("Here too.");
//    	return "list";
//    }
//
//    @GetMapping("/search-query")
//    public String searchQuery(org.springframework.ui.Model model,
//			@RequestParam(value = "inputtext", required = false) String inputtext,
//    		HttpServletRequest request) {
//    	model.addAttribute("sourceText", inputtext);
//    	WMDocument qd = null;
//    	String annotatedText = null;
//    	model.addAttribute("annotatedText", annotatedText);
//    	return "resultsearch";
//    }
//
//    @GetMapping("/results")
//    public String results(org.springframework.ui.Model model,HttpServletRequest request) {
//    	return "resultsearch";
//    }
//
//    @GetMapping("/results3")
//    public String results3(org.springframework.ui.Model model,HttpServletRequest request) {
//    	return "results3";
//    }
//
//    @GetMapping("/document")
//    public String document(org.springframework.ui.Model model,
//			@RequestParam(value = "documentId", required = false) String documentId,
//    		HttpServletRequest request) {
//    	return "document";
//    }

}
